﻿using System;
using System.Diagnostics;
using System.Text.Json;

namespace Serializer
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            var result = "";
            var options = new JsonSerializerOptions { IncludeFields = true };

            var f = F.Get();

            var timePerParse = Stopwatch.StartNew();
            for (i = 0; i < 100000; i++)
            {
                result = CsvSerializer.SerializeFields(f);
            }
            timePerParse.Stop();
            WriteResult($"Execute csv serialize {i} times:", result, timePerParse.ElapsedMilliseconds);

            var csv = "i1,5\ni2,4\ni3,3\ni4,2\ni5,1";
            timePerParse = Stopwatch.StartNew();
            for (i = 0; i < 100000; i++)
            {
                f = CsvSerializer.DeserializeFields<F>(csv);
            }
            timePerParse.Stop();
            result = CsvSerializer.SerializeFields(f);
            WriteResult($"Execute csv deserialize {i} times:", result, timePerParse.ElapsedMilliseconds);

            timePerParse = Stopwatch.StartNew();
            for (i = 0; i < 100000; i++)
            {
                result = JsonSerializer.Serialize(f, typeof(F), options);
            }
            timePerParse.Stop();
            WriteResult($"Execute JsonSerialize {i} times:", result, timePerParse.ElapsedMilliseconds);

            timePerParse = Stopwatch.StartNew();
            Console.Write($"{result}");
            timePerParse.Stop();
            WriteResult($"Execute Console.Write:", "", timePerParse.ElapsedMilliseconds);
        }

        static void WriteResult(string header, string result, long ms)
        {
            Console.WriteLine($"{header}");
            Console.WriteLine($"{result}");
            Console.WriteLine($"Milliseconds: {ms}");
            Console.WriteLine();
        }
    }

    class F
    {
        public int i1, i2, i3, i4, i5;
        public static F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }
}