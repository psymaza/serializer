using System;
using System.Linq;
using System.Reflection;

namespace Serializer
{
    public static class CsvSerializer
    {
        private static BindingFlags Flags => BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

        public static string SerializeFields(object obj)
        {
            var fields = obj.GetType().GetFields(Flags);

            var fieldArray = (from f in fields
                select $"{f.Name},{f.GetValue(obj).ToString()}");

            return string.Join("\n", fieldArray);
        }

        public static T DeserializeFields<T>(string csv) where T : new()
        {
            var values = csv.Split('\n');

            var instance = Activator.CreateInstance<T>();
            foreach (var value in values)
            {
                var arr = value.Split(',');
                if (!arr.Any())
                    continue;
                var field = typeof(T).GetField(arr[0], Flags);

                if (field != null)
                {
                    field.SetValue(instance, Convert.ChangeType(arr[1], field.FieldType));
                }
            }

            return instance;
        }
    }
}